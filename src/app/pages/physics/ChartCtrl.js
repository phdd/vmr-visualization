(function () {
  'use strict';

  angular.module('BlurAdmin.pages.physics')
    .controller('ChartCtrl', ChartCtrl);

  var DEFAULT_HOST = "http://192.168.12.17:3700";

  var defaultSeriesStyle = {
    lineWidth: 1.1,
    strokeStyle: '#209e91',
    fillStyle: 'rgba(175,246,239,0.30)'
  };

  var defaultChartStyle = {
    grid: {
      fillStyle: '#ffffff',
      strokeStyle: 'rgba(119,119,119,0.25)',
      sharpLines: true,
      millisPerLine: 5000,
      borderVisible: false
    },
    labels: {
      fillStyle: 'rgba(0,0,0,0.61)'
    },

    interpolation: 'bezier',
    maxDataSetLength: 100000,
    timestampFormatter: SmoothieChart.timeFormatter,
    maxValueScale: 1.01,
    minValueScale: 1.01
  };

  /** @ngInject */
  function ChartCtrl($rootScope, $scope, $element, $attrs, localStorageService) {
    if (typeof $attrs.min !== 'undefined')
      defaultChartStyle.minValue = parseFloat($attrs.min);

    if (typeof $attrs.max !== 'undefined')
      defaultChartStyle.maxValue = parseFloat($attrs.max);

    if (typeof $attrs.verticalSections !== 'undefined')
      defaultChartStyle.grid.verticalSections = parseInt($attrs.verticalSections);

    var smoothie = new SmoothieChart(defaultChartStyle),
      series = new TimeSeries(),
      socket = null,
      panel = $element.find(".panel-body"),
      canvas = $element.find("canvas");

    $scope.value = '0';
    series.append(moment().valueOf(), 0);
    canvas.attr('width', panel.width());

    var chartHeight = localStorageService.get("settings.appearance.chartHeight");
    if (angular.isDefined(chartHeight) && chartHeight != null)
      canvas.attr('height', chartHeight);

    smoothie.streamTo(canvas[0], 0);
    smoothie.addTimeSeries(series, defaultSeriesStyle);

    var connect = function(server) {
      if (!angular.isDefined(server) || server == null)
        return;

      if (socket != null && socket.connected)
        socket.disconnect();

      socket = io(server)
        .on('connect', function () {
          socket.emit('monitor', $attrs.node);
        })
        .on('message', function (data) {
          var value = Math.round(data.value * 100) / 100;
          series.append(moment().valueOf(), value);
          $scope.$apply(function () { $scope.value = '' + value; })
        });
    };

    $rootScope.$on("LocalStorageModule.notification.setitem", function(e, data) {
      if (data.key == 'settings.source.host')
        connect(data.newValue);
    });

    if (localStorageService.get("settings.source.host") == null)
      localStorageService.set("settings.source.host", DEFAULT_HOST)

    connect(localStorageService.get("settings.source.host"));
  }

})();
