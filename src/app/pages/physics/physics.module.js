(function () {
  'use strict';

  angular.module('BlurAdmin.pages.physics', [])
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('physics', {
        url: '/physics',
        templateUrl: 'app/pages/physics/physics.html',
        title: 'Physical Context',
        sidebarMeta: {
          icon: 'ion-earth',
          order: 100
        }
      });
  }

})();
