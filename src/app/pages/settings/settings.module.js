(function () {
  'use strict';

  angular.module('BlurAdmin.pages.settings', [])
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('settings', {
        url: '/settings',
        templateUrl: 'app/pages/settings/settings.html',
        title: 'Settings',
        sidebarMeta: {
          icon: 'ion-settings',
          order: 100
        }
      });
  }

})();