(function () {
  'use strict';

  angular.module('BlurAdmin.pages.settings')
    .controller('SettingsCtrl', SettingsCtrl);

  /** @ngInject */
  function SettingsCtrl($scope, localStorageService) {
    var saveSource = function () {
      $scope.source.successMessage = false;
      $scope.source.failureMessage = false;

      var socket = io($scope.source.host, { 'reconnection': false }),
        failureHandler = function () {
          $scope.$apply(function () {
            $scope.source.failureMessage = true;
          });
        },
        successHandler = function () {
          socket.disconnect();
          $scope.$apply(function () {
            $scope.source.successMessage = true;
            localStorageService.set("settings.source.host", $scope.source.host);
          });
        };

      socket.on('connect', successHandler);
      socket.on('connect_error', failureHandler);
      socket.on('connect_timeout', failureHandler);
    };

    $scope.source = {
      successMessage: false,
      failureMessage: false,
      save:           saveSource,
      host:           localStorageService.get("settings.source.host")
    };

    var saveAppearance = function () {
      localStorageService.set("settings.appearance.chartHeight", $scope.appearance.chartHeight);
    };

    $scope.appearance = {
      save:        saveAppearance,
      chartHeight: localStorageService.get("settings.appearance.chartHeight")
    };
  }

})();
