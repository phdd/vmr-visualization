var express = require("express"),
    opcua = require("node-opcua"),
    async = require("async");

var port = 3700,
    endpoint = "opc.tcp://localhost:26543",
    client = new opcua.OPCUAClient({ keepSessionAlive: true }),
    app = require('express')(),
    http = require('http').Server(app),
    io = require('socket.io')(http),
    server = {
      session: null,
      subscription: null
    };

var createRoomFor = function(node) {
  console.log("creating room for " + node);
  server.subscription.monitor({
      nodeId: node,
      attributeId: 13
    }, {
      samplingInterval: 800,
      discardOldest: true,
      queueSize: 500
    }, opcua.read_service.TimestampsToReturn.Both, function (error) {
      if (error) throw error;
    })

    .on("changed", function (changeEvent) {
      if (changeEvent.value == null) return;
      io.to(node).emit("message", {
        value: changeEvent.value.value,
        timestamp: changeEvent.serverTimestamp,
        browseName: changeEvent.browseName,
        nodeId: node
      });
    });
};

var hasRoomFor = function(node) {
  return typeof io.sockets.adapter.rooms[node] !== "undefined"
};

io.on("connection", function (socket) {
  socket.on("monitor", function (node) {
    if (!hasRoomFor(node)) createRoomFor(node);
    socket.join(node);
  });
});

async.series([
  function (callback) {
    client.connect(endpoint, callback);
  },
  function (callback) {
    client.createSession(null, function (error, session) {
      if (!error) server.session = session;
      callback(error);
    });
  },
  function (callback) {
    server.subscription = new opcua.ClientSubscription(server.session, {
      requestedPublishingInterval: 1000,
      requestedMaxKeepAliveCount: 1000,
      requestedLifetimeCount: 6000,
      maxNotificationsPerPublish: 1,
      publishingEnabled: true,
      publishingInterval: 100,
      priority: 10
    });

    server.subscription.on("started", function () {
      callback();
    })
  }
], function (error) {
  if (error) throw error;
  http.listen(port);
});
